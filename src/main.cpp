#include <iostream>
#include "lib/euclid.h"

using namespace std;

int main()
{
    int first, second;

    cout << "Print two integers\n";
    cout << "First: ";
    cin >> first;
    cout << "Second: ";
    cin >> second;
    cout << "\nTheir GCD is " << euclid(first, second);

    return 0;
}
