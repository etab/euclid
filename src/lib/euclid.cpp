//euclid library implements an Euclidian algorithm for integers

#include "euclid.h"

//swaps two integers
static void swap(int &first, int &second)
{
    int interim = second;
    second = first;
    first = interim;
}

//implementation of the algorithm
int euclid(int first, int second)
{
    //gcd of any number and zero is that number
    if((first == 0) or (second == 0))
        return (first + second);

    //ordering integers
    if(second > first)
        swap(first, second);

    int reminder;
    do
    {
        reminder = first % second;
        first = second;
        second = reminder;
    }while(reminder != 0);

    if(first < 0)
        first = -first;

    return first;
}
