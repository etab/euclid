//This is a header file for the euclid library
//euclid library implements an Euclidian algorithm for integers

#ifndef EUCLID_H
#define EUCLID_H

int euclid(int first, int second);

#endif // EUCLID_H
